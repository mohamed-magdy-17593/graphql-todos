import React from 'react'
import styled from '@emotion/styled'
import {dark, white} from '../styles/variables'
import {centerContent} from '../styles/common'

const NavEl = styled.nav(centerContent, {
  height: 70,
  backgroundColor: dark,
  color: white,
})

function Nav() {
  return (
    <NavEl>
      <div>
        GraphQL <strong>TODOS</strong>
      </div>
    </NavEl>
  )
}

export default Nav
