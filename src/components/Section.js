import React from 'react'
import styled from '@emotion/styled'
import {DragDropContext, Droppable, Draggable} from 'react-beautiful-dnd'
import {
  spaceLg,
  space,
  width,
  borderRadius,
  primary,
  spaceSm,
  darkRed,
} from '../styles/variables'
import {centerContent} from '../styles/common'
import {FaTimes} from 'react-icons/fa'

const Section = styled.section(centerContent, {
  margin: `${spaceLg}px 0`,
  padding: space,
})

const Item = styled.div({
  maxWidth: width,
  padding: space,
  borderRadius: borderRadius,
  backgroundColor: primary,
  margin: `${spaceSm}px 0`,
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
})

const Button = styled.button(centerContent, {
  cursor: 'pointer',
  border: 0,
  padding: space,
  borderRadius: borderRadius,
  background: 'transparent',
  color: darkRed,
})

const ITEMS = [
  {id: 1, text: 'What is this'},
  {
    id: 2,
    text: `
      Lorem ipsum dolor sit amet consectetur, adipisicing elit. Expedita,
      deleniti officia quaerat ab sit aliquid placeat amet rerum nam
      reprehenderit facere atque animi ullam eos aliquam. Aliquam eligendi
      pariatur nam!
    `,
  },
  {id: 3, text: 'Honda beatch'},
  {
    id: 4,
    text: `
      officia quaerat ab sit aliquid placeat amet rerum nam
      reprehenderit facere
    `,
  },
]

function TodosSection() {
  return (
    <DragDropContext onDragEnd={r => {}}>
      <Section>
        <Droppable droppableId={'todos'}>
          {({droppableProps, innerRef, placeholder}) => (
            <div ref={innerRef} {...droppableProps}>
              {ITEMS.map((item, i) => (
                <React.Fragment key={item.id}>
                  <Draggable draggableId={item.id} index={i}>
                    {({draggableProps, dragHandleProps, innerRef}) => (
                      <Item
                        ref={innerRef}
                        {...draggableProps}
                        {...dragHandleProps}
                      >
                        <div contentEditable>{item.text}</div>
                        <Button onClick={e => {}}>
                          <FaTimes />
                        </Button>
                      </Item>
                    )}
                  </Draggable>
                </React.Fragment>
              ))}
              {placeholder}
            </div>
          )}
        </Droppable>
      </Section>
    </DragDropContext>
  )
}

export default TodosSection
