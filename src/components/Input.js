import React from 'react'
import styled from '@emotion/styled'
import {
  primary,
  space,
  dark,
  fontSizeLg,
  borderSizeLg,
  borderRadius,
  spaceLg,
  width,
  white,
  darkRed,
} from '../styles/variables'
import {applyStylesWhenPropExest} from '../styles/helpers'
import {centerContent} from '../styles/common'

const InputContainer = styled.div({
  position: 'relative',
  margin: `0 ${space}px`,
  width: '100%',
  maxWidth: width,
})

const Input = styled.input(
  {
    padding: space,
    paddingRight: 55,
    border: `${borderSizeLg}px solid ${dark}`,
    borderRadius: borderRadius,
    fontSize: fontSizeLg,
    width: '100%',
    outline: 'none',
    boxShadow: `0 0 ${spaceLg}px rgba(0, 0, 0, 0.2)`,
    ':focus': {
      border: `${borderSizeLg}px solid ${primary}`,
    },
    ':focus + span': {
      backgroundColor: primary,
    },
  },
  applyStylesWhenPropExest({
    error: {
      ':focus': {
        border: `${borderSizeLg}px solid ${darkRed}`,
      },
      ':focus + span': {
        backgroundColor: darkRed,
      },
    },
  }),
)

const Span = styled.span(centerContent, {
  backgroundColor: dark,
  width: 35,
  height: 35,
  borderRadius: '50%',
  color: white,
  position: 'absolute',
  top: 12,
  right: 20,
})

function InputWithCounter(props) {
  const length = props.value.length
  return (
    <InputContainer>
      <Input error={!length} {...props} />
      <Span>{length}</Span>
    </InputContainer>
  )
}

export default InputWithCounter
