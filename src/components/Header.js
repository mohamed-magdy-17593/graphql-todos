import React, {useState} from 'react'
import styled from '@emotion/styled'
import {centerContent} from '../styles/common'
import doodlesBg from './doodles.png'
import InputWithCounter from './Input'

const Hero = styled.header(centerContent, {
  height: 250,
  backgroundImage: `url(${doodlesBg})`,
  backgroundPosition: 'center',
})

function useInput(initialValue = '') {
  const [value, setValue] = useState(initialValue)
  const handleChange = e => setValue(e.target.value)
  return {value, setValue, handleChange}
}

function Header() {
  const {value: todo, handleChange: handleTodoChange, setValue} = useInput('')
  return (
    <Hero>
      <form
        style={{display: 'contents'}}
        onSubmit={e => {
          e.preventDefault()
          if (!todo) return
          console.log(todo)
          setValue('')
        }}
      >
        <InputWithCounter
          placeholder="What todo"
          value={todo}
          onChange={handleTodoChange}
        />
      </form>
    </Hero>
  )
}

export default Header
