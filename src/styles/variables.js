import css from '@emotion/css'

const importFont = css`
  @import url('https://fonts.googleapis.com/css?family=Raleway');
`
const font = "'Raleway', sans-serif"

const fontSizeSm = '0.8rem'
const fontSize = '1rem'
const fontSizeLg = '1.2rem'

const white = '#fff'
const lightGray = '#999'
const darkBlueGray = '#303952'
const lightBlueGray = '#596275'
const darkYellow = '#f5cd79'
const lightYellow = '#f7d794'
const darkBnafsigy = '#574b90'
const darkRed = '#e15f41'
const lightRed = '#e77f67'
const primary = lightYellow
const dark = darkBlueGray

const borderSize = 1
const borderSizeLg = 3
const borderRadius = 5

const spaceSm = 8
const space = 15
const spaceLg = 30

const width = 700

export {
  importFont,
  font,
  fontSizeSm,
  fontSize,
  fontSizeLg,
  darkBlueGray,
  lightBlueGray,
  darkYellow,
  lightYellow,
  darkRed,
  lightRed,
  darkBnafsigy,
  primary,
  dark,
  white,
  lightGray,
  borderSize,
  borderSizeLg,
  borderRadius,
  spaceSm,
  space,
  spaceLg,
  width,
}
