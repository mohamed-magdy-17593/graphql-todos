function applyStylesWhenPropExest(propsStyles) {
  return props =>
    Object.entries(propsStyles).reduce(
      (stylesArray, [prop, propStyle]) => [
        ...stylesArray,
        props[prop] ? propStyle : null,
      ],
      [],
    )
}

export {applyStylesWhenPropExest}
