import React from 'react'
import {Global} from '@emotion/core'
import {importFont, font, dark, lightGray, fontSize} from './styles/variables'
import Nav from './components/Nav'
import Header from './components/Header'
import Section from './components/Section'

function GlobalStyles() {
  return (
    <Global
      styles={[
        importFont,
        {
          '*': {
            fontFamily: font,
            boxSizing: 'border-box',
          },
          body: {
            margin: 0,
            padding: 0,
            color: dark,
            fontSize: fontSize,
          },
          '::placeholder': {
            color: lightGray,
          },
        },
      ]}
    />
  )
}

function App() {
  return (
    <div>
      <GlobalStyles />
      <Nav />
      <Header />
      <Section />
    </div>
  )
}

export default App
